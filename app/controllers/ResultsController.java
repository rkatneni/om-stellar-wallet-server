package controllers;

import com.fasterxml.jackson.databind.node.ObjectNode;
import models.StellarUser;
import play.Logger;
import play.data.Form;
import play.data.validation.Constraints;
import play.libs.F;
import play.libs.Json;
import play.mvc.*;

import static play.libs.Json.toJson;
import static play.mvc.Controller.request;
import static play.mvc.Controller.response;

public class ResultsController extends Controller {

    public static final String VALIDITY_STATUS = "status";

    public static Result showuser() {
        response().setHeader("Access-Control-Allow-Origin","*");
    	Form<ShowUser> showuserForm = Form.form(ShowUser.class).bindFromRequest();
    	
    	if (showuserForm.hasErrors()) {
            return badRequest(showuserForm.errorsAsJson());
        }

        ShowUser showuser = showuserForm.get();

        ObjectNode responseJson = Json.newObject();
        String statusString = "failure";
        if(StellarUser.checkIfUsernameUpdatetokenExists(showuser.username, showuser.updateToken)){
        	statusString = "success";
            ObjectNode dataJson = Json.newObject();
            dataJson.put("inviteCode","");
            dataJson.put("claimedInviteCode",false);
            dataJson.put("invites","[]");
            ObjectNode emailJson = Json.newObject();
            emailJson.put("address","wuyanna2009@gmail.com");
            emailJson.put("verified",1);
            dataJson.put("email",emailJson);
            dataJson.put("linkedFacebook",true);
            responseJson.put("data",dataJson);
        }

        
        responseJson.put(VALIDITY_STATUS,statusString);

        return ok(responseJson);
    }

    public static class ShowUser {
    	@Constraints.Required
    	public String username;

        @Constraints.Required
        public String updateToken;
    }

}