package controllers;

import com.fasterxml.jackson.databind.node.ObjectNode;
import models.StellarUser;
import play.Logger;
import play.data.Form;
import play.data.validation.Constraints;
import play.libs.F;
import play.libs.Json;
import play.mvc.*;

import static play.libs.Json.toJson;
import static play.mvc.Controller.request;
import static play.mvc.Controller.response;

public class ValidityController extends Controller {

    public static final String VALIDITY_STATUS = "status";
    public static final String VALIDITY_CODE = "code";
    public static final String VALIDITY_MESSAGE = "message";

    public static Result validname() {
        response().setHeader("Access-Control-Allow-Origin","*");
    	Form<Validname> validnameForm = Form.form(Validname.class).bindFromRequest();
    	
    	if (validnameForm.hasErrors()) {
            return badRequest(validnameForm.errorsAsJson());
        }

        Validname validname = validnameForm.get();
        System.out.println("validname - " + validname.username);
        String statusString = "success";
        ObjectNode responseJson = Json.newObject();            
        if(StellarUser.checkIfUsernameExists(validname.username)){
        	statusString = "fail";
            responseJson.put(VALIDITY_CODE,"already_taken");
            responseJson.put(VALIDITY_MESSAGE,"This username is taken.");
            responseJson.put(VALIDITY_STATUS,statusString);
            return badRequest(responseJson);
        }else{
            responseJson.put(VALIDITY_STATUS,statusString);
            return ok(responseJson);
        }
    }

    public static Result federation() {
        response().setHeader("Access-Control-Allow-Origin","*");
        Form<Federation> federationForm = Form.form(Federation.class).bindFromRequest();
        
        if (federationForm.hasErrors()) {
            return badRequest(federationForm.errorsAsJson());
        }

        Federation federation = federationForm.get();

        String destAddress = StellarUser.checkIfOmletIdExists(federation.omletId);
        System.out.println("omletId - " + federation.omletId + " dest addr: " + destAddress);
        String destination = federation.destination;
        String domain = federation.domain;
        String type= federation.type;
        ObjectNode responseJson = Json.newObject();
        if(destAddress != null){
            ObjectNode federationJson = Json.newObject();
            federationJson.put("destination_address",destAddress);
            federationJson.put("destination",destination);
            federationJson.put("domain",domain);
            federationJson.put("type","federation_record");
            federationJson.put("stellar_user", StellarUser.findUsernameByOmletId(federation.omletId));
            responseJson.put("federation_json",federationJson);
            return ok(responseJson);
        }

                    
        responseJson.put("result","error");
        responseJson.put("error","noSuchUser");
        responseJson.put("error_message","No user found");
        return ok(responseJson);
    }


    public static Result revfederation() {
        response().setHeader("Access-Control-Allow-Origin","*");
        Form<ReverseFederation> revfederationForm = Form.form(ReverseFederation.class).bindFromRequest();
        
        if (revfederationForm.hasErrors()) {
            return badRequest(revfederationForm.errorsAsJson());
        }

        ReverseFederation revfederation = revfederationForm.get();

        String destination = StellarUser.getDestination(revfederation.destination_address);
        String destAddress = revfederation.destination_address;
        String domain = revfederation.domain;
        String type= revfederation.type;
        ObjectNode responseJson = Json.newObject();
        if(destination != null){
            ObjectNode federationJson = Json.newObject();
            federationJson.put("destination",destination);
            federationJson.put("domain",domain);
            federationJson.put("type","federation_record");
            federationJson.put("destination_address",destAddress);
            responseJson.put("federation_json",federationJson);
            return ok(responseJson);
        } 

                    
       responseJson.put("result","error");
        responseJson.put("error","noSuchUser");
        responseJson.put("error_message","No user found");
        return ok(responseJson);
        
    }

    public static class Validname {
    	@Constraints.Required
    	public String username;
    }

    public static class Federation {
        @Constraints.Required
        public String destination;
         @Constraints.Required
        public String domain;
         @Constraints.Required
        public String type;
        @Constraints.Required
        public String omletId;
    }

    public static class ReverseFederation {
        @Constraints.Required
        public String destination_address;
         @Constraints.Required
        public String domain;
         @Constraints.Required
        public String type;
    }

}