package controllers;

import com.fasterxml.jackson.databind.node.ObjectNode;
import models.User;
import models.StellarUser;
import play.Logger;
import play.data.Form;
import play.data.validation.Constraints;
import play.libs.F;
import play.libs.Json;
import play.mvc.*;
import java.util.regex.Pattern;

import static play.libs.Json.toJson;
import static play.mvc.Controller.request;
import static play.mvc.Controller.response;

public class SecurityController extends Controller {

    public final static String AUTH_TOKEN_HEADER = "X-AUTH-TOKEN";
    public static final String AUTH_TOKEN = "authToken";
    public static final String AUTH_STATUS = "status";
    public static final String RESULT_WALLET = "wallet";
    public static final String RESULT_HEADER = "result";
    public static final String RESULT_DATA_HEADER = "data";
    public static final Pattern p = Pattern.compile("[^a-zA-Z0-9]");


    public static User getUser() {
        return (User)Http.Context.current().args.get("user");
    }

    // returns an authToken
    public static Result login() {
        Form<Login> loginForm = Form.form(Login.class).bindFromRequest();

        if (loginForm.hasErrors()) {
            return badRequest(loginForm.errorsAsJson());
        }

        Login login = loginForm.get();

        System.out.println(login.email+login.password);

        User user = User.findByEmailAddressAndPassword(login.email, login.password);

        if (user == null) {
            String statusString = "fail";
            ObjectNode authTokenJson = Json.newObject();
            authTokenJson.put(AUTH_STATUS,statusString);
            response().setHeader("Access-Control-Allow-Origin","*");
            return ok(authTokenJson);
            //return unauthorized();
        }
        else {
            String statusString = "success";
            String authToken = user.createToken();
            String walletString = "gUuM2jUW8ifGZMm2tFdnkfcxsyDqmup5XD";
            ObjectNode resultJson = Json.newObject();
            resultJson.put(RESULT_WALLET,walletString);            
            ObjectNode authTokenJson = Json.newObject();            
            authTokenJson.put(AUTH_TOKEN, authToken);
            authTokenJson.put(AUTH_STATUS,statusString);
            authTokenJson.put(RESULT_HEADER,resultJson);
            response().setCookie(AUTH_TOKEN, authToken);
            response().setHeader("Access-Control-Allow-Origin","*");
            return ok(authTokenJson);
        }
    }
    // returns a Status
    public static Result signup() {
        Form<SignUp> signUpForm = Form.form(SignUp.class).bindFromRequest();

        if (signUpForm.hasErrors()) {
            return badRequest(signUpForm.errorsAsJson());
        }

        SignUp login = signUpForm.get();

        User user = new User(login.email,login.password,login.fullName, login.username) ;


        if (user == null) {
            return unauthorized();
        }
        else {
            user.save();
            //String authToken = user.createToken();
            ObjectNode resultJson = Json.newObject();
            resultJson.put(AUTH_STATUS, "Registered");
            //response().setCookie(AUTH_TOKEN, authToken);
            response().setHeader("Access-Control-Allow-Origin","*");
            return ok(resultJson);
        }
    }

    public static Result register() {
        response().setHeader("Access-Control-Allow-Origin","*");
        Form<Register> registerForm = Form.form(Register.class).bindFromRequest();
        if (registerForm.hasErrors()) {
            return badRequest(registerForm.errorsAsJson());
        }
        
        Register register = registerForm.get();
        int usernameLength = register.username.length();
        if ( 4 > usernameLength || usernameLength > 11){
            return badRequest("Invalid username");
        } else if (p.matcher(register.address).find() && register.address.length() > 40) {
            return badRequest("Invalid address");
        }
        StellarUser stellaruser = new StellarUser(register.username, register.address, register.omletId);

        String authToken = stellaruser.getAuthToken();
        String updateToken = stellaruser.getUpdateToken();
        Long userId = stellaruser.getUserId();
        String omletId = stellaruser.getOmletId();


        ObjectNode resDataJson = Json.newObject();
        resDataJson.put("username",register.username);
        resDataJson.put("address",register.address);
        resDataJson.put("recaptchaResponse","...");
        resDataJson.put("fromIP","130.65.251.53");
        resDataJson.put("userAgent","Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.115 Safari/537.36");
        resDataJson.put("updateToken",updateToken);
        resDataJson.put("authToken",authToken);
        resDataJson.put("userID",userId);
        resDataJson.put("omletId",omletId);
        
        ObjectNode resultJson = Json.newObject();
        resultJson.put(AUTH_STATUS, "success");
        resultJson.put(RESULT_DATA_HEADER,resDataJson);

        return ok(resultJson);
    }


    

    @Security.Authenticated(Secured.class)
    public static Result logout() {
        response().discardCookie(AUTH_TOKEN);
        getUser().deleteAuthToken();
        return redirect("/");
    }

    public static class Login {

        @Constraints.Required
        @Constraints.Email
        public String email;

        @Constraints.Required
        public String password;

        @Constraints.Required
        public String username;

    }

    public static class SignUp {

    @Constraints.Required
    @Constraints.Email
    public String email;

    @Constraints.Required
    public String password;

    @Constraints.Required
    public String fullName;

    @Constraints.Required
    public String username;

    }

    public static class Register {

    @Constraints.Required
    public String username;

    @Constraints.Required
    public String address;

    @Constraints.Required
    public String omletId;

    }

    

}
