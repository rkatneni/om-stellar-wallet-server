package controllers;

import play.mvc.Controller;
import play.mvc.Result;

public class Application extends Controller {

	
	public static Result options(String path) {
		response().setHeader("Access-Control-Allow-Origin","*");
		response().setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
		response().setHeader("Access-Control-Allow-Headers", "Accept, Origin, Content-type, X-Json, X-Prototype-Version, X-Requested-With");
		response().setHeader("Access-Control-Allow-Credentials", "true");
		response().setHeader("Access-Control-Max-Age", "86400");
		return ok("");
	}

}
