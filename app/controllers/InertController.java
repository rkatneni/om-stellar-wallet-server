package controllers;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import models.StellarUser;
import play.Logger;
import play.data.Form;
import play.data.validation.Constraints;
import play.libs.F;
import play.libs.Json;
import play.mvc.*;

import static play.libs.Json.toJson;
import static play.mvc.Controller.request;
import static play.mvc.Controller.response;

public class InertController extends Controller {

    public static final String VALIDITY_STATUS = "status";

    public static Result fairy() {
        response().setHeader("Access-Control-Allow-Origin","*");
        ObjectNode responseJson = Json.newObject();
        String statusString = "success";
        ObjectNode dataJson = Json.newObject();
        ObjectNode federationJson = Json.newObject();
        federationJson.put("destination","StellarFoundation");
        federationJson.put("domain","stellar.org");
        federationJson.put("type","federation_record");
        federationJson.put("destination_address","gDSSa75HPagWcvQmwH7D51dT5DPmvsKL4q");
        dataJson.put("federation_json",federationJson);
        responseJson.put("data",dataJson);

        responseJson.put(VALIDITY_STATUS,statusString);
        return ok(responseJson);
    }

    public static Result rewards() {
        response().setHeader("Access-Control-Allow-Origin","*");
        ObjectNode responseJson = Json.newObject();
        String statusString = "success";
        //String[] rewards = new String[0];
        ObjectNode dataJson = Json.newObject();
        ObjectNode rewards = Json.newObject();
        ArrayNode rew = rewards.arrayNode();
        dataJson.put("giveawayAmount","4000");
        dataJson.put("rewards",rewards);
        responseJson.put("status",statusString);
        responseJson.put("data",dataJson);
        
        return ok(responseJson);
    }
}