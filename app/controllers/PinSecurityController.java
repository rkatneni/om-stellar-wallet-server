package controllers;

import com.fasterxml.jackson.databind.node.ObjectNode;
import models.User;
import models.StellarUser;
import models.StellarWalletUser;
import play.Logger;
import play.data.Form;
import play.data.validation.Constraints;
import play.libs.F;
import java.util.Date;
import play.libs.Json;
import play.mvc.*;
import java.util.regex.Pattern;

import static play.libs.Json.toJson;
import static play.mvc.Controller.request;
import static play.mvc.Controller.response;

public class PinSecurityController extends Controller {

    public final static String AUTH_TOKEN_HEADER = "X-AUTH-TOKEN";
    public static final String AUTH_TOKEN = "authToken";
    public static final String AUTH_STATUS = "status";
    public static final String RESULT_WALLET = "wallet";
    public static final String RESULT_HEADER = "result";
    public static final String RESULT_DATA_HEADER = "data";
    public static final Pattern p = Pattern.compile("[^a-zA-Z0-9]");


    

    // returns an authToken
    public static Result pinLogin() {
        response().setHeader("Access-Control-Allow-Origin","*");
        Form<PinLogin> loginForm = Form.form(PinLogin.class).bindFromRequest();

        if (loginForm.hasErrors()) {
            return badRequest(loginForm.errorsAsJson());
        }

        PinLogin login = loginForm.get();

        System.out.println("pinlogin: "+login.username+","+login.device+","+login.lookup);
        String uname = login.username;
        String d = login.device;
        StellarWalletUser loginattempt = StellarWalletUser.findencryptedWallet(login.username, login.device,login.lookup);

        if (loginattempt == null) {
            String statusString = "fail";
            StellarWalletUser updateStatus = StellarWalletUser.updateActiveWallet(uname, d);
            //System.out.println(updateStatus.getactiveStatus());
            
            if (updateStatus != null){  // Wrong pin
                Date d1 = updateStatus.getmodifiedDate();
                Date d2 = new Date();
                long diff = d2.getTime() - d1.getTime();
                long diffHours = diff / (60 * 60 * 1000) ;
                System.out.println("diffHours "+ diffHours);
                if (updateStatus.getactiveStatus() == 4 && diffHours < 24){
                    updateStatus.setactiveStatus(-1);
                    updateStatus.modifiedDate = new Date();

                }else if (diffHours < 24){
                     updateStatus.setactiveStatus(updateStatus.getactiveStatus() +1);
                    updateStatus.modifiedDate = new Date();
                }else if (diffHours >= 24){
                    updateStatus.setactiveStatus(1);
                    updateStatus.modifiedDate = new Date();
                }
                updateStatus.update();
                System.out.println("Wrong pin");
                return badRequest("Wrong pin");
            } else {    // pin deactivated or pin doesn't exist
                System.out.println("pin deactivated");
                return badRequest("Your pin is deactivated. Please use password to login.");
            }
            
        } 
        else {
            String statusString = "success";
            if (loginattempt.getactiveStatus() != 0){
                loginattempt.setactiveStatus(0);
                loginattempt.modifiedDate = new Date();
                loginattempt.update();
            }
           // String authToken = user.createToken();
           // String walletString = encryptedWalletId;
            ObjectNode resultJson = Json.newObject();
            resultJson.put("encryptedWallet",loginattempt.getencryptedWallet());            
            ObjectNode authTokenJson = Json.newObject();            
           // authTokenJson.put(AUTH_TOKEN, authToken);
            authTokenJson.put(AUTH_STATUS,statusString);
            authTokenJson.put(RESULT_DATA_HEADER,resultJson);
            return ok(authTokenJson);
        }
    }
    

    public static Result pinRegister() {
        response().setHeader("Access-Control-Allow-Origin","*");
        Form<PinRegister> pinregisterForm = Form.form(PinRegister.class).bindFromRequest();
        System.out.println("ab");
        if (pinregisterForm.hasErrors()) {
            return badRequest(pinregisterForm.errorsAsJson());
        }
        
        PinRegister pinregister = pinregisterForm.get();
        int usernameLength = pinregister.username.length();
        if ( 4 > usernameLength || usernameLength > 11){
            return badRequest("Invalid username");
        } 
        System.out.println(pinregister.username+","+pinregister.device+","+pinregister.lookup+","+pinregister.encryptedWallet);
        StellarWalletUser stellaruser = new StellarWalletUser(pinregister.username, pinregister.device, pinregister.lookup,pinregister.encryptedWallet);
        stellaruser.save();
        
        Long userId = stellaruser.getUserId();
        String username = stellaruser.getUsername();
        String device = stellaruser.getdevice();
        String lookup = stellaruser.getlookup();
    
        String encryptedWallet = stellaruser.getencryptedWallet();


        ObjectNode resDataJson = Json.newObject();
        resDataJson.put("username",pinregister.username);
       
        resDataJson.put("recaptchaResponse","...");
        resDataJson.put("fromIP","130.65.251.53");
        resDataJson.put("userAgent","Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.115 Safari/537.36");
        resDataJson.put("device",device);
        resDataJson.put("lookup",lookup);
        resDataJson.put("userID",userId);
        resDataJson.put("encryptedWallet",encryptedWallet);
        
        ObjectNode resultJson = Json.newObject();
        resultJson.put(AUTH_STATUS, "success");
        resultJson.put(RESULT_DATA_HEADER,resDataJson);

        return ok(resultJson);
    }

    public static Result pinExists() {
        response().setHeader("Access-Control-Allow-Origin","*");
        Form<PinExists> pinexistsForm = Form.form(PinExists.class).bindFromRequest();
        
        if (pinexistsForm.hasErrors()) {
            return badRequest(pinexistsForm.errorsAsJson());
        }   

        PinExists pinexist = pinexistsForm.get();
       String statusString = "success";
        StellarWalletUser sWuser = StellarWalletUser.updateActiveWallet(pinexist.username, pinexist.device);
        
        if (sWuser == null) {
            statusString = "fail";
        }
       

        
        ObjectNode authTokenJson = Json.newObject();
        authTokenJson.put(AUTH_STATUS,statusString);
            
        return ok(authTokenJson);
    }
    
    public static Result pinChange() {
	System.out.println("pin change attemp");
        response().setHeader("Access-Control-Allow-Origin","*");
        Form<PinChange> pinchangeForm = Form.form(PinChange.class).bindFromRequest();
        System.out.println("pinchange");
        if (pinchangeForm.hasErrors()) {
	    System.out.println("bad req: for has errors");
            return badRequest(pinchangeForm.errorsAsJson());
        }

        PinChange pinchange = pinchangeForm.get();
        Boolean userexists = StellarUser.checkIfUsernameUpdatetokenExists(pinchange.username,pinchange.updateToken);
        if (userexists == false){
	    System.out.println("invalid user");
            return badRequest("Invalid Details");
        }
        
        String d = pinchange.device;
        String uname = pinchange.username;
        System.out.println("pin change: " + uname + "  " + d);
        StellarWalletUser sWuser = StellarWalletUser.updateActiveWallet(uname, d);
        Long userId = null;
        if (sWuser == null) {
           StellarWalletUser swu = new StellarWalletUser(pinchange.username, pinchange.device, pinchange.lookup,pinchange.encryptedWallet);
            swu.save();
            
            userId = swu.getUserId();
        }else{
            sWuser.setactiveStatus(1);
            sWuser.setmodifiedDate(new Date());
            sWuser.update();
            StellarWalletUser swu = new StellarWalletUser(pinchange.username, pinchange.device, pinchange.lookup,pinchange.encryptedWallet);
            swu.save();
            userId = swu.getUserId();
        }
        
        String username = pinchange.username;
        String device = pinchange.device;
        String lookup = pinchange.lookup;
    
        String encryptedWallet = pinchange.encryptedWallet;


        ObjectNode resDataJson = Json.newObject();
        resDataJson.put("username",pinchange.username);
       
        resDataJson.put("recaptchaResponse","...");
        resDataJson.put("fromIP","130.65.251.53");
        resDataJson.put("userAgent","Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.115 Safari/537.36");
        resDataJson.put("device",device);
        resDataJson.put("lookup",lookup);
        resDataJson.put("userID",userId);
        resDataJson.put("encryptedWallet",encryptedWallet);
        
        ObjectNode resultJson = Json.newObject();
        resultJson.put(AUTH_STATUS, "success");
        resultJson.put(RESULT_DATA_HEADER,resDataJson);
        return ok(resultJson);
        
    }

    public static class PinLogin {

        @Constraints.Required
        public String username;

        @Constraints.Required
        public String device;

        @Constraints.Required
        public String lookup;

    }

    

    public static class PinRegister {

    @Constraints.Required
    public String username;

    @Constraints.Required
    public String device;

    @Constraints.Required
    public String lookup;

    @Constraints.Required
    public String encryptedWallet;

    }

    

    public static class PinChange {

    @Constraints.Required
    public String username;

    @Constraints.Required
    public String updateToken;

    @Constraints.Required
    public String device;

    @Constraints.Required
    public String lookup;

    @Constraints.Required
    public String encryptedWallet;

    }

    public static class PinExists {

    @Constraints.Required
    public String username;

    

    @Constraints.Required
    public String device;

    

    }

}
