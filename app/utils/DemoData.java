package utils;

import models.*;
import play.Logger;

import java.util.ArrayList;

public class DemoData {

    public static User user1;
    public static User user2;

    public static StellarUser stellaruser1;
    public static StellarUser stellaruser2;

    public static StellarWalletUser stellarwaluser1;
    public static StellarWalletUser stellarwaluser2;

    public static Todo todo1_1;
    public static Todo todo1_2;

    public static Todo todo2_1;

    public static void loadDemoData() {
        
        Logger.info("Loading Demo Data");

        user1 = new User("user1@demo.com", "password", "John Doe", "user1");
        user1.save();
        
        todo1_1 = new Todo(user1, "make it secure");
        todo1_1.save();

        todo1_2 = new Todo(user1, "make it neat");
        todo1_2.save();

        user2 = new User("user2@demo.com", "password", "Jane Doe", "user2");
        user2.save();

        todo2_1 = new Todo(user2, "make it pretty");
        todo2_1.save();

        stellaruser1 = new StellarUser("rahul","xf84XXkkkm3hIW0s9FeO6sfvjiPt9vT8E1","8765432345");
        stellaruser1.save();
        stellaruser2 = new StellarUser("megha","pU8Xudhl7JDJqxp93dh44ZFckk4iVkwhI1","abcd@gmail.com");
        stellaruser2.save();

        stellarwaluser1 = new StellarWalletUser("r","xf84XXkkkm3hIW0s9FeO6sfvjiPt9vT8E1","8765432345","ewreret");
        stellarwaluser1.save();
        stellarwaluser2 = new StellarWalletUser("m","pU8Xudhl7JDJqxp93dh44ZFckk4iVkwhI1","abcd@gmail.com","srere");
        stellarwaluser2.save();

    }

}
