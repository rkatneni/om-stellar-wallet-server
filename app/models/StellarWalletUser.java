package models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import org.apache.commons.codec.binary.Base32;

@Table(
    uniqueConstraints=
        @UniqueConstraint(columnNames={"username", "device","active_status","creation_date","lookup"})
)

@Entity
public class StellarWalletUser extends Model {

	@ManyToOne
    @Id
    public Long id;
    public Long getUserId() {
        return id;
    }

    @Column(length = 10,   nullable = false)
    @Constraints.MaxLength(10)
    @Constraints.Required
    private String username;

    public String getUsername() {
        return username;
    }
    public void setUsername(String username1) {
        this.username = username1.toLowerCase();
    }
    

    @Column(length = 512,  nullable = false)
    @Constraints.MaxLength(512)
    @Constraints.Required
    private String device;

    public String getdevice() {
        return device;
    }

    public void setdevice(String device1) {
        this.device = device1;
    }

    

    @Column(length=512,  nullable= false)
    @Constraints.Required

    private String lookup;

    public String getlookup() {
        return lookup;
    }
    public void setlookup(String lookup1) {
        this.lookup = lookup1;
    }

    @Column(length=512,  nullable= false)
    @Constraints.Required

    private String encryptedWallet;

    public String getencryptedWallet() {
        return encryptedWallet;
    }
    public void setencryptedWallet(String encryptedWallet1) {
        this.encryptedWallet = encryptedWallet1;
    }

    @Column(nullable=false)
    public int activeStatus;
    public int getactiveStatus() {
        return activeStatus;
    }
    public void setactiveStatus(int status) {
        this.activeStatus = status;
    }

    @Column(nullable = false)
    public Date creationDate;
    public void setCreationDate() {
        if (this.creationDate == null){
            this.creationDate = new Date();
        }
    }

    @Column(nullable = false)
    public Date modifiedDate;
    public Date getmodifiedDate() {
        return modifiedDate;
    }
    public void setmodifiedDate(Date d) {
        this.modifiedDate = d;
    }
   

    

    public StellarWalletUser() {
        this.creationDate = new Date();
        this.modifiedDate = new Date();
        this.activeStatus = 0;
    }

    public StellarWalletUser(String username, String device, String lookup,String encryptedWallet) {
        setdevice(device);
        setUsername(username);
        setlookup(lookup);
        setencryptedWallet(encryptedWallet);
        setCreationDate();
        this.modifiedDate = new Date();
        setactiveStatus(0);
        
    }

    

    public static Finder<Long, StellarWalletUser> find = new Finder<Long, StellarWalletUser>(Long.class, StellarWalletUser.class);

    

    public static StellarWalletUser updateActiveWallet(String username,String device){
        try {
            System.out.println(" u "+ username + " " + device);
            return find.where().eq("username", username).eq("device",device).findUnique();
            
        }
        catch (Exception e) {
            return null;
        }
    }

    
    

    public static StellarWalletUser findencryptedWallet(String username,String device, String lookup){
        try {
            return find.where().eq("username", username).eq("device",device).eq("lookup", lookup).gt("activeStatus",-1).findUnique();
               
        }
        catch (Exception e) {
            return null;
        }
    }

    

}