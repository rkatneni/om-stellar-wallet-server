package models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import org.apache.commons.codec.binary.Base32;

@Entity
public class StellarUser extends Model {

	@Id
    public Long id;
    public Long getUserId() {
        return id;
    }

    private String authToken;
    public String getAuthToken() {
        return authToken;
    }
    @Column(length = 255)
    private String updateToken;
    public String getUpdateToken() {
        return updateToken;
    }

    @Column(length = 255, unique = true, nullable = false)
    @Constraints.MaxLength(255)
    @Constraints.Required
    private String address;

    public String getAddress() {
        return address;
    }

    @Column(length = 10, unique = true, nullable = false)
    @Constraints.MaxLength(10)
    @Constraints.Required
    private String username;

    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username.toLowerCase();
    }

    @Column(length=100, unique = true, nullable= false)
    @Constraints.Required

    private String omletid;

    public String getOmletId() {
        return omletid;
    }
    public void setOmletId(String omletid) {
        this.omletid = omletid;
    }

    @Column(nullable = false)
    public Date creationDate;

    public boolean createTokens() {
    	String saltprefix = "CMUOmlet";
    	String baseString = saltprefix + this.username;
    	byte[] toEncoded = getSha512(baseString);
    	Base32 base32 = new Base32();
    	updateToken = new String(base32.encode(toEncoded));
        authToken = UUID.randomUUID().toString();
        save();
        return true;
    }

    public void deleteAuthToken() {
        authToken = null;
        save();
    }

    public StellarUser() {
        this.creationDate = new Date();
    }

    public StellarUser(String username, String address, String omletId) {
        this.address = address;
        setUsername(username);
        setOmletId(omletId);
        this.creationDate = new Date();
        createTokens();
    }

    public static byte[] getSha512(String value) {
        try {
            return MessageDigest.getInstance("SHA-512").digest(value.getBytes("UTF-8"));
        }
        catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
        catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public static Finder<Long, StellarUser> find = new Finder<Long, StellarUser>(Long.class, StellarUser.class);

    public static StellarUser findByAuthToken(String authToken) {
        if (authToken == null) {
            return null;
        }

        try  {
            return find.where().eq("authToken", authToken).findUnique();
        }
        catch (Exception e) {
            return null;
        }
    }

    public static boolean checkIfUsernameExists(String username){
        if(find.where().eq("username", username).findUnique() != null){
            return true;    
        }
        return false;
    }

    public static boolean checkIfUsernameUpdatetokenExists(String username, String updateToken){
        System.out.println("username "+ username + "updateToken "+ updateToken);
        if(find.where().eq("username", username).eq("update_token", updateToken).findUnique() != null){
            return true;    
        }
        return false;
    }

    public static String checkIfOmletIdExists(String omletId){
        try {
            return find.where().eq("omletid", omletId).findUnique().getAddress() ;
               
        }
        catch (Exception e) {
            return null;
        }
    }

    public static String findUsernameByOmletId(String omletId){
        try {
            return find.where().eq("omletid", omletId).findUnique().getUsername() ;
               
        }
        catch (Exception e) {
            return null;
        }
    }

    public static String getDestination(String destAddress){
        try {
            return find.where().eq("address", destAddress).findUnique().getOmletId() ;
               
        }
        catch (Exception e) {
            return null;
        }
    }

}