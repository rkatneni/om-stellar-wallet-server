# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table stellar_user (
  id                        bigint auto_increment not null,
  auth_token                varchar(255),
  update_token              varchar(255),
  address                   varchar(255) not null,
  username                  varchar(10) not null,
  omletid                   varchar(100) not null,
  creation_date             datetime not null,
  constraint uq_stellar_user_address unique (address),
  constraint uq_stellar_user_username unique (username),
  constraint uq_stellar_user_omletid unique (omletid),
  constraint pk_stellar_user primary key (id))
;

create table stellar_wallet_user (
  id                        bigint auto_increment not null,
  username                  varchar(10) not null,
  device                    varchar(512) not null,
  lookup                    varchar(512) not null,
  encrypted_wallet          varchar(512) not null,
  active_status             integer not null,
  creation_date             datetime not null,
  modified_date             datetime not null,
  constraint uq_stellar_wallet_user_1 unique (username,device,active_status,creation_date,lookup),
  constraint pk_stellar_wallet_user primary key (id))
;

create table todo (
  id                        bigint auto_increment not null,
  value                     varchar(1024) not null,
  user_id                   bigint,
  constraint pk_todo primary key (id))
;

create table user (
  id                        bigint auto_increment not null,
  auth_token                varchar(255),
  email_address             varchar(255) not null,
  username                  varchar(10) not null,
  sha_password              varbinary(64) not null,
  full_name                 varchar(255) not null,
  creation_date             datetime not null,
  constraint uq_user_email_address unique (email_address),
  constraint uq_user_username unique (username),
  constraint pk_user primary key (id))
;

alter table todo add constraint fk_todo_user_1 foreign key (user_id) references user (id) on delete restrict on update restrict;
create index ix_todo_user_1 on todo (user_id);



# --- !Downs

SET FOREIGN_KEY_CHECKS=0;

drop table stellar_user;

drop table stellar_wallet_user;

drop table todo;

drop table user;

SET FOREIGN_KEY_CHECKS=1;

