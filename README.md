# Omlet Wallet API Server #

## Setup your environment ##
* [Install Play framework](https://www.playframework.com/)
* Setup eclipse project

```
#!java

play eclipse
```
* Install MySQL
* Setup MySQL
```
#!SQL

CREATE USER 'omstellaradmin'@'localhost' IDENTIFIED BY 'givemethemoney!';

GRANT ALL PRIVILEGES ON * . * TO 'omstellaradmin'@'localhost';
```


## Start the server ##

```
#!bash

activator run

```