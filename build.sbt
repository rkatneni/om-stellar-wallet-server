import play.PlayJava

val appName         = "cmu-mobisocial-stellar-project"
val appVersion      = "1.0"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.11.1"

libraryDependencies ++= Seq(
  javaCore,
  javaJdbc,
  cache,
  javaEbean
)

libraryDependencies += "mysql" % "mysql-connector-java" % "5.1.34"
